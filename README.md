Clearhaus API
=============
Lightweight wrapper for the Clearhaus REST API.

Installation
------------

You can install clearhaus-api using Composer:

```
composer require magelabs/clearhaus-api
```
